import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { TaxiApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MapComponent } from '../components/map/map';
import { GeocoderService } from '../providers/map/geocoder.service';
import { MapService } from '../providers/map/map.service';

@NgModule({
  declarations: [
    TaxiApp,
    HomePage,   
    MapComponent
  ],
  imports: [
    IonicModule.forRoot(TaxiApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    TaxiApp,
    HomePage
  ],
  providers: [ GeocoderService, MapService],
})
export class AppModule {
}
